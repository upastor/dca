import sys

religion = "" if len(sys.argv) < 1 else str(sys.argv[1])
greetings = {
    'cristiano': "Feliz navidad",
    'judio': 'Feliz hanukah',
    'ateo': 'Feliz cumpleaños de Isaac Newton'
}
try:
    greet = greetings[religion]
    print(greet)
except:
    print('Que tenga un buen día')
